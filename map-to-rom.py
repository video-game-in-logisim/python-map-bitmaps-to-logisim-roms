#!/usr/bin/env python

import sys
from PIL import Image

img = Image.open(sys.argv[1])  # open the image given in the command

# as the ROM is designed so that each address is a column, rotate the image so that when it is flattened, pixels from the same column are next to each other
img = img.transpose(Image.ROTATE_270)

flattenedImg = img.getdata()
binary = ''

# for each pixel in the flattened bitmap
for pixel in flattenedImg:
    if pixel[0] == 0:
        # append 0 to the binary string if current pixel is not lit
        binary += '0'
    else:
        # append 1 to the binary string if current pixel is not lit
        binary += '1'

# convert the string of binary digits into an equivalent hex string
binary_int = int(binary, 2)
hex_str = str(hex(binary_int))

# chop off the the 0x from the start of the hex string
hex_str = hex_str[2:]

# space every 8 values or one memory address
hex_str = " ".join(hex_str[i:i+8] for i in range(0, len(hex_str), 8))

f = open(sys.argv[1] + '.txt', 'w')
f.write('v2.0 raw\n')
f.write(hex_str)